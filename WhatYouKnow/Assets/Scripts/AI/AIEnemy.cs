﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIEnemy : MonoBehaviour {

	// Movement 
	[Tooltip("List of patrol points for this enemy")]
	public GameObject[] patrolPoints;
	[Tooltip("The tolerance distance to which the patrol point is reached")]
	public float patrolTargetTolerance = 0.5f;
	[HideInInspector] public int patrolDestinationIndex = 0;
	private NavMeshAgent agent;

	// State
	AIState state;

	// Use this for initialization
	void Start () {

		// Initialize to patrol state
		state = new PatrolState (this);

		agent = gameObject.GetComponent<NavMeshAgent> ();
		agent.autoBraking = false;
	}
	
	// Update is called once per frame
	void Update () {
		state.StateUpdate ();
	}

	public NavMeshAgent Agent {
		get {
			return this.agent;
		}
	}
}
