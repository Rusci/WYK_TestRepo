﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIState {

	protected AIEnemy ower;

	public AIState (AIEnemy ower) {
		this.ower = ower;
	}

	abstract public void StateUpdate ();
}
