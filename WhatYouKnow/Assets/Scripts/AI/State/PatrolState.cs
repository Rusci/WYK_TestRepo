﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : AIState {

	public PatrolState (AIEnemy ower) : base (ower) {}

	public override void StateUpdate () {
		Patrol ();
	}

	private void Patrol () {
		if (!ower.Agent.pathPending && ower.Agent.remainingDistance < 0.5f) {
			GoToNextPatrolPoint ();
		}
	}

	private void GoToNextPatrolPoint() {

		// Returns if no points have been set up
		if (ower.patrolPoints.Length == 0) {
			Debug.Log (ower.gameObject.name + ": Has no patrol points!");
			return;
		}

		if (ower.Agent == null) {
			Debug.Log (ower.gameObject.name + ": Has no navMeshAgent !");
			return;
		}

		// Set target to go to currently selected destination
		Vector3 patrolPointPosition = ower.patrolPoints [ower.patrolDestinationIndex].transform.position;
		ower.Agent.destination = patrolPointPosition;

		// Choose the next points in the array as destination
		ower.patrolDestinationIndex = (ower.patrolDestinationIndex + 1) % ower.patrolPoints.Length;
	}
}
