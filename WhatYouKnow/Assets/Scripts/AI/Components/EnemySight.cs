﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour {

	public Material patrolling;
	public Material caution;
	public Material alert;

	public GameObject player;
	public float cooldownAlert;
	public float cooldownCaution;

	private GameObject enemy;
	private Renderer enemySkin;

	private bool sawPlayer = false;
	private bool lostPlayer = false;

	void Awake(){
		
		enemy = transform.parent.gameObject;
		enemySkin = enemy.GetComponent<Renderer> ();
	}

	void Update(){

		if (lostPlayer == true && cooldownAlert > 0f) {
			if (cooldownAlert > 0f) {
				cooldownAlert -= Time.deltaTime;
				Debug.Log ("" + cooldownAlert);
			} 
		}
		else if(cooldownAlert < 0f) {
			lostPlayer = false;
			enemySkin.material = patrolling;
			sawPlayer = false;
		}
	}

	void OnTriggerStay(Collider other){

		if (other.gameObject == player) {

			Vector3 direction = other.transform.position - transform.position;
			RaycastHit hit;

			Debug.DrawRay (transform.position, direction, Color.cyan);

			if (Physics.Raycast (transform.position, direction, out hit)) {

				if (hit.collider.gameObject == player) {

					if (hit.distance < 4f) {
						
						enemySkin.material = alert;
						sawPlayer = true;
						cooldownAlert = 5f;
					} 
					else if (hit.distance > 4f && sawPlayer == false) {
						enemySkin.material = caution;
						cooldownCaution = 5f;
					}

				} 
			}
			
		}
	}

	void OnTriggerExit(Collider other){
		
		if (other.gameObject == player) {

			lostPlayer = true;
		}
	}

}
