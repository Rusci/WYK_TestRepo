﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float walkSpeed;
	public float runSpeed;
	public float crouchSpeed;
	public float crouchHeight;

	private float moveForward;
	private float moveSide;
	private float originalHeight;
	private CapsuleCollider thisCollider;

	private bool isColliding = false;
	private bool isCrouched = false;

	// Use this for initialization
	void Start () {

		Cursor.lockState = CursorLockMode.Locked;
		thisCollider = gameObject.GetComponent<CapsuleCollider> ();
		originalHeight = thisCollider.height;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetButton ("Crouch")){
			Crouch (crouchHeight);
			isCrouched = true;
		}

		if(Input.GetButton ("Crouch") == false){

			if (isColliding == true) {
				Crouch (crouchHeight);
			} 
			else {
				Crouch (originalHeight);
				isCrouched = false;
			}
		}
			
		if (Mathf.Abs(Input.GetAxis ("MoveForward")) != 0 || Mathf.Abs(Input.GetAxis ("MoveSide")) != 0) {

			if (isCrouched == true) {
				Movement (crouchSpeed);
			}
			else if (Input.GetButton ("Run")) {
				Movement (runSpeed);
			}
			else {
				Movement (walkSpeed);
			}
		} 

		moveForward *= Time.deltaTime;
		moveSide *= Time.deltaTime;
	}

	void FixedUpdate(){

		transform.Translate (moveSide, 0, moveForward);
	}

	void Crouch(float height){

		thisCollider.height = Mathf.Lerp (thisCollider.height, height, 0.05f);

		RaycastHit hit;
		Vector3 direction = transform.TransformDirection (Vector3.up);
		if (Physics.Raycast (transform.position, direction, out hit, 1)) {
		
			if (hit.collider != null && hit.collider.gameObject.layer != 8) {
				isColliding = true;
			} 
		}
		else {
			isColliding = false;
		}
	}

	void Movement(float speed){
		moveForward = Input.GetAxis ("MoveForward") * speed;
		moveSide = Input.GetAxis ("MoveSide") * speed;
	}

}
