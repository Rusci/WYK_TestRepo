﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabberComponent : MonoBehaviour {

	Camera cameraRef;
	Transform cam;
	private GameObject carriedObject;
	private bool isCarring;
	private float current;

	public bool enableComponent;
	public float objSpeedRot;
	public float maxDist;
	public float minDist;
	public int repulsion;

	// Use this for initialization
	void Start () {

		enableComponent = true;
		isCarring = false;
		cameraRef = gameObject.GetComponent<Camera> ();
		cam = gameObject.transform;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (enableComponent == true) 
		{
			if (Input.GetButtonDown ("Grab") || isCarring == true) {

				Grab ();
			}
			if (Input.GetButtonUp ("Grab") && carriedObject != null) {
				
				DetachObject (false, null, null, true, false);
			}
			if (Input.GetButtonDown ("Throw") && isCarring == true) {

				Throw ();
			}
			if (Input.GetButton ("RotateObject") && isCarring == true) {

				Rotate (Input.GetAxis ("RotateObjectX"), Input.GetAxis ("RotateObjectY"));
			}
			if (Input.GetButtonUp ("RotateObject")) {

				cameraRef.GetComponent<CameraController> ().enableCamera = true;
			}
			if ((Input.GetAxis ("RotatePadY") != 0 || Input.GetAxis ("RotatePadX") != 0) && isCarring == true) {

				Rotate (Input.GetAxis ("RotatePadX"), Input.GetAxis ("RotatePadY"));
			} 
			if (Input.GetAxis ("RotatePadY") == 0 && Input.GetAxis ("RotatePadX") == 0 && Input.GetButton ("RotateObject") == false) {
				
				cameraRef.GetComponent<CameraController> ().enableCamera = true;
			}
		}
	}
	// Function to Hanlde the Grab
	void Grab(){
		
		// Hanlde case when player handle something
		if (cameraRef != null) {

			// Create RayCast
			Ray ray = cameraRef.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0));
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, maxDist)) {

				float distance = Vector3.Distance (gameObject.transform.parent.position, hit.point);

				if (hit.collider.gameObject.layer == 8 && distance > minDist) {
					
					// Attach Object
					if (isCarring == false) {
						
						carriedObject = hit.collider.gameObject;
						carriedObject.transform.parent = cam;
						current = hit.distance + 0.5f;
						isCarring = true;
					}
					carriedObject.GetComponent<Rigidbody> ().useGravity = false;
					carriedObject.GetComponent<Rigidbody> ().freezeRotation = true;

					// Avoid repositioning bug when player try to grab an falling object
					carriedObject.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezePosition;
					carriedObject.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.None;

					// Handle case when alredy carried an object but the raycast hit another one that can be carried
					if (carriedObject != hit.collider.gameObject) {
						DetachObject (false, null, null, true, false);
					}
					// Use Zoom
					if (Input.GetAxis ("ZoomObject") != 0 || isCarring == true) {
						Zoom (Input.GetAxis ("ZoomObject"), ray.GetPoint (current));
					}
				}
				// Loose object if you are pointing to another one
				else {
					if (carriedObject != null) {
						DetachObject (false, null, null, true, false);
					}
				}
			}
			// Handle case when player lost object
			else {
				if (carriedObject != null) {
					DetachObject (false, null, null, true, false);
				}
			}
		} 
		// No camera has been found in the gameobject
		else {
			Debug.Log ("Any camera component has been found!");
		}
	}
	// Function to Zoom on an object
	void Zoom(float zoomValue, Vector3 Vec){
		
		carriedObject.transform.position = Vector3.Lerp(carriedObject.transform.position, Vec, Time.deltaTime * 3f);
		current += Input.GetAxis ("ZoomObject");
	}
	// Function to throw object
	void Throw(){
		
		carriedObject.GetComponent<Rigidbody> ().AddForce (cam.forward * repulsion);
		DetachObject (false, null, null, true, false);
	}
	// Function to rotate the object and disable camera movement
	void Rotate(float rotX, float rotY){

		cameraRef.GetComponent<CameraController> ().enableCamera = false;

		rotX = rotX * objSpeedRot;
		rotY = rotY * objSpeedRot;

		carriedObject.transform.Rotate (Vector3.up, rotX * Time.deltaTime);
		carriedObject.transform.Rotate (Vector3.right, rotY * Time.deltaTime);
	}
	// Detach an object from the player
	void DetachObject(bool isAttached, GameObject objectRef, Transform attachTo, bool enableGravity, bool disableRotation){
		
			isCarring = isAttached;
			carriedObject.transform.parent = attachTo;
			carriedObject.GetComponent<Rigidbody> ().useGravity = enableGravity;
			carriedObject.GetComponent<Rigidbody> ().freezeRotation = disableRotation;
			carriedObject = objectRef;
			cameraRef.GetComponent<CameraController> ().enableCamera = true;
	}
}